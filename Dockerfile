# syntax=docker/dockerfile:1

ARG openjdk_version=14
ARG alpine_version=3.14.0

# Internal, passed between stages.
ARG build_date
ARG java_dir=/opt/java-minimal
ARG jena_version=""
ARG fuseki_dir=/fuseki
ARG fuseki_jar=jena-fuseki-server-${jena_version}.jar
ARG vcs_ref

## ---- Stage: Download and build java.
FROM openjdk:${openjdk_version}-alpine AS builder

ARG java_dir
ARG jena_version
ARG fuseki_dir
ARG fuseki_jar
# local build args
ARG repo=https://repo1.maven.org/maven2
ARG jar_url=${repo}/org/apache/jena/jena-fuseki-server/${jena_version}/${fuseki_jar}

# check if jena_version is set
RUN [ "${jena_version}" != "" ] || { echo && echo '**** Set jena_version ****' && echo ; exit 1 ; } \
  && echo && echo "==== Docker build for Apache Jena Fuseki ${jena_version} ====" && echo

# For objcopy used in jlink; ignore version pinning because we are in build stage.
# hadolint ignore=DL3018
RUN apk add --no-cache curl binutils

WORKDIR $fuseki_dir

# Copy tools.
COPY tools/* ./

# Download the jar with check of the SHA1 checksum.
RUN ./download --chksum sha1 "$jar_url"

# Build a reduced Java JDK.
ARG jdeps_extra="jdk.crypto.cryptoki,jdk.crypto.ec"
# Create a class file for the HealthCheck script.
RUN javac HealthCheck.java \
  # Get dependencies from Fuseki jar.
  && jdeps="$(jdeps --multi-release base --print-module-deps --ignore-missing-deps ${fuseki_jar})" \
  # Get HealtchCheck dependencies from class file.
  && health_deps="$(jdeps --multi-release base --print-module-deps --ignore-missing-deps HealthCheck.class)" \
  && jlink \
    --compress 2 --strip-debug --no-header-files --no-man-pages \
    --output "${java_dir}" \
    --add-modules "${jdeps},${jdeps_extra},${health_deps}"


FROM alpine:${alpine_version} as fuseki-prod

ARG build_date
ARG debug_flag
ARG jena_version
ARG java_dir
ARG fuseki_dir
ARG fuseki_jar
ARG vcs_ref

# LABELS
LABEL org.opencontainers.image.authors="Stefan Hynek <stefan.hynek@uni-goettingen.de>"
LABEL org.opencontainers.image.created="${build_date}"
LABEL org.opencontainers.image.description="Production image for fuseki ${jena_version}."
LABEL org.opencontainers.image.documentation="https://gitlab.gwdg.de/sepia/fuseki-images/-/blob/main/README.md"
LABEL org.opencontainers.image.licenses="AGPL-3.0-or-later"
LABEL org.opencontainers.image.revision="${vcs_ref}"
LABEL org.opencontainers.image.source="https://gitlab.gwdg.de/sepia/fuseki-images.git"
LABEL org.opencontainers.image.title="fuseki-images"
LABEL org.opencontainers.image.url="https://gitlab.gwdg.de/sepia/fuseki-images"
LABEL org.opencontainers.image.vendor="sepia"
LABEL org.opencontainers.image.version="${jena_version}+${vcs_ref}${debug_flag}"


COPY --from=builder ${java_dir} ${java_dir}
COPY --from=builder ${fuseki_dir}/${fuseki_jar} ${fuseki_dir}/fuseki.jar

WORKDIR $fuseki_dir

# apply logging configuration
COPY conf/log4j2.prod log4j2.properties

## Default environment variables.
ENV PATH=${java_dir}/bin:$PATH \
  JAVA_HOME=${java_dir}

EXPOSE 3030

# Provide a java-based HEALTHCHECK.
COPY --from=builder ${fuseki_dir}/HealthCheck.* ./
HEALTHCHECK --interval=6s --timeout=1s --retries=5 CMD ["java", "HealthCheck", "http://localhost:3030/$/ping", "||", "exit", "1"]

# Enable "--ping" to make healthcheck work.
ENTRYPOINT ["java", "-jar", "/fuseki/fuseki.jar", "--ping"]
# Provide a non-persisting in-memory database as default.
CMD ["--mem", "/ds"]


FROM fuseki-prod as fuseki-debug

ARG build_date
ARG debug_flag
ARG jena_version
ARG vcs_ref

LABEL org.opencontainers.image.created="${build_date}"
LABEL org.opencontainers.image.description="Development image for fuseki ${jena_version}."
LABEL org.opencontainers.image.version="${jena_version}+${vcs_ref}${debug_flag}"

COPY conf/log4j2.debug log4j2.properties
