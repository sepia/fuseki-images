/** Java-based health check for an HTTP endpoint.
 * Derived from and inspired by [Naiyer Asif](https://mflash.dev/blog/2021/03/01/java-based-health-check-for-docker/#docker-healthcheck-instruction-using-a-single-file-java-program)
 */
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;

public class HealthCheck {

  public static void main(String[] args) throws InterruptedException, IOException {
    // first argument should be the endpoint the request is sent to
    String arg1 = args[0];
    var client = HttpClient.newHttpClient();
    var request = HttpRequest.newBuilder()
        .uri(URI.create(arg1))
        .header("accept", "text/plain")
        .build();

    var response = client.send(request, BodyHandlers.ofString());

    if (response.statusCode() != 200) {
      throw new RuntimeException("Healthcheck failed");
    }
  }
}
