# fuseki-images

The build process described in this repository is derived from the [Apache Jena Fuseki Docker Tools](https://repo1.maven.org/maven2/org/apache/jena/jena-fuseki-docker/) but implements certain improvements.

## Objectives

1. Provide a container image build of the most recent Apache Jena Fuseki release.

    The Apache Jena project does not provide container images but a [Dockerfile and hints](https://jena.apache.org/documentation/fuseki2/fuseki-docker.html
) on how to build it. I took notice of [Stian Soiland-Reyes's approach](https://github.com/stain/jena-docker) but found it not sufficiently following best practices.

1. Unify logging output in production.

    As the logging configuration could be a matter of discussion, two proposals reside in this repository (see `conf/`) and are built into the respective container images.

1. Follow several best practices regarding `Dockerfile`s, container image builds and container image security.
    - [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
    - [Best practices for scanning images](https://docs.docker.com/develop/scan-images/)
    - [Intro Guide to Dockerfile Best Practices](https://www.docker.com/blog/intro-guide-to-dockerfile-best-practices/)
    - [Best practices for building containers](https://cloud.google.com/architecture/best-practices-for-building-containers)
    - [10 Docker Security Best Practices](https://snyk.io/blog/10-docker-image-security-best-practices/)

## Updates

The parent image is alpine 3.14 with an announced EOL of 2023-05-01. To update the parent image, change the CI variable `alpine_version` in `.gitlab-ci.yml:17`. For updating Fuseki itself, change `jena_version` in `.gitlab-ci.yml:19`

## Usage

---

:warning: Disclaimer: This fuseki image build is unconfigured in terms of access control, so **DO NOT** expose its service to the public. However, if no configuration is provided, fuseki will use secure defaults that allow read-only access from `localhost` only.
If you want to [configure access control](https://jena.apache.org/documentation/fuseki2/fuseki-security.html) and other security features, bind mount a `shiro.ini` to `/fuseki`. Also, see [Data Access Control for Fuseki](https://jena.apache.org/documentation/fuseki2/fuseki-data-access-control.html) and [Configuring Fuseki](https://jena.apache.org/documentation/fuseki2/fuseki-configuration.html).

---

In a `Dockerfile`:

```Dockerfile
FROM docker.gitlab.gwdg.de/sepia/fuseki-images/fuseki:4.1.0-5ef8f6fb
```

In development, always use the debugging image with the same version/hash combination as you are going to use in production to prevent unexpected outcome. If you would use the above image in production, use the following for development:

```Dockerfile
FROM docker.gitlab.gwdg.de/sepia/fuseki-images/fuseki:4.1.0-5ef8f6fb-debug
```

## Development

1. Install dependencies.

    ```sh
    npm ci
    ```

1. Code.

1. Commit (`git commit`).

## Remarks considering code quality

This repo is [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) and uses [husky🐶](https://www.npmjs.com/package/husky) to configure pre-commit `Dockerfile` linting with [hadolint](https://github.com/hadolint/hadolint) (needs docker-cli to be installed).
